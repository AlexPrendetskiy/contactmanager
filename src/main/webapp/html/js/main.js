var MainCtrl = function (config) {
    this.containerId = config.containerId;
    this.container = $('#' + this.containerId);

    this.deleteDialog = config.deleteDialog;

    // Buttons
    this.saveBtnId = config.saveBtn;
    this.deleteBtnId = config.deleteBtn;

    // Forms
    this.form = $('#' + config.formId);
    this.searchForm = config.searchForm;

    // Loading animation
    this.loading = $('#' + config.loading);

    // Urls
    this.deleteUrl =  config.deleteUrl;
    this.redirectUrl =  config.redirectUrl;
    this.searchUrl =  config.searchUrl;

    scope = this;
};

$.extend(MainCtrl, true, {

    prototype: {

        initialize: function () {
            this.container.delegate('#' + this.deleteDialog , 'click', this.openDeleteDialog);
            this.container.delegate('#' + this.saveBtnId, 'click', this.saveBtnHandler);
            this.container.delegate('#' + this.deleteBtnId , 'click', this.deleteBtnHandler);
            this.container.delegate('#' + this.searchForm , 'keypress', this.search);
        },

        openDeleteDialog: function (e) {
            $('#' + scope.deleteDialog).attr('href', '#confirmationDialog');
        },

        search: function (e) {
            const ENTER = 13;
            var keycode = (e.keyCode ? e.keyCode : e.which);
            if (keycode == ENTER) {
                alert('NOT IMPLEMENTED YET!!! BUT you can use such url "/actinities/search/{query}" or "/contacts/search/{query}" ');
            }
        },

        saveBtnHandler: function (e) {
            e.preventDefault();

            scope.form.submit();

        },

        deleteBtnHandler: function (e) {
            e.preventDefault();

            var contact = $('#' + scope.deleteDialog).attr('data-id');

            scope.loading.show();

            $.post(scope.deleteUrl, {id: contact})
                .done(function (response) {
                    window.location.href = scope.redirectUrl;
                })
                .fail(function (error) {
                    window.location.href = scope.redirectUrl;
            });
        }
    }

});

window.MainCtrl = MainCtrl;