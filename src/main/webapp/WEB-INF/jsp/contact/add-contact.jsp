<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="container">
    <div class="row">
        <c:if test="${contactId ne null}">
            <a href="javascript:;" id="deleteDialog" data-id="${contactId}" data-toggle="modal"  class="btn btn-danger"><spring:message code="delete.contact"/></a>
        </c:if>
        <a href="javascript:;" id="save-contact" class="btn btn-info"><spring:message code="save.contact"/></a>
    </div>
    <br/>
    <div class="row">
        <form:form id="add-contact-form" modelAttribute="contact" action="/save-or-update-contact" method="post">
            <form:hidden path="id"/>

            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="contact.first.name"/> *</label>
                <div class="col-sm-4">
                    <form:input path="firstName" cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="contact.last.name"/> *</label>
                <div class="col-sm-4">
                    <form:input path="lastName" cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="contact.email"/> *</label>
                <div class="col-sm-4">
                    <form:input path="emailAddress" cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="contact.telephone"/></label>
                <div class="col-sm-4">
                    <form:input path="phoneNumber" cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="contact.address.1"/></label>
                <div class="col-sm-4">
                    <form:input path="address1" cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="contact.address.2"/></label>
                <div class="col-sm-4">
                    <form:input path="address2" cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="contact.city"/></label>
                <div class="col-sm-4">
                    <form:input path="city" cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="contact.post.code"/></label>
                <div class="col-sm-4">
                    <form:input path="postCode" cssClass="form-control"/>
                </div>
            </div>

            <c:if test="${contact.id ne null}">
                <div class="form-group row">
                    <a href="${pageContext.servletContext.contextPath}/activities/add-activity-to-contact/${contact.id}"
                       class="btn btn-info"><spring:message code="add.activity"/></a>
                    <br/>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message code="activity.title"/></th>
                            <th><spring:message code="activity.type"/></th>
                            <th><spring:message code="activity.due.date"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${contact.activities}" var="activity">
                            <tr>
                                <td>${activity.title}</td>
                                <td>${activity.activityType}</td>
                                <td><fmt:formatDate pattern="dd-MM-yyyy" value="${activity.dueDate}"/>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:if>

        </form:form>
    </div>


    <div id="confirmationDialog" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><spring:message code="delete.contact" /></h4>
          </div>
          <div class="modal-body">
            <p><spring:message code="delete.contact.confirmation.message" /></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="no" /></button>
            <button type="button" id="delete-contact" data-dismiss="modal" class="btn btn-default"><spring:message code="yes" /></button>
          </div>
        </div>

      </div>
    </div>

</div>
