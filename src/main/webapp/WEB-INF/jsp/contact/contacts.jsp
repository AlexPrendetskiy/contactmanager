<%@include file="contact-init.jsp" %>

<div id="contact-container" class="container">

    <div class="row">
        <div class="col-md-4">
            <div class="row form-group">
                <input type="text" value="" id="search-contact" class="form-control" placeholder="<spring:message code="search"/>"/>
            </div>
            <div class="row form-group">
                <span class="list-group-item active"><spring:message code="name"/></span>
                <c:forEach items="${contacts}" var="contact">
                    <a href="${pageContext.servletContext.contextPath}/contacts/${contact.id}" class="list-group-item">${contact.firstName} ${contact.lastName}</a>
                </c:forEach>
            </div>
            <div class="row">
                <a href="${pageContext.servletContext.contextPath}/contacts/add-contact" id="add-contact" class="btn btn-info"><spring:message code="add.contact"/></a>
            </div>
        </div>
        <div class="col-md-8">
            <c:if test="${showAddContactView}">
                <%@include file="add-contact.jsp" %>
            </c:if>
        </div>

    </div>
</div>
