<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="${pageContext.servletContext.contextPath}/html/js/main.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        var config = {
            containerId: 'contact-container',
            deleteDialog: 'deleteDialog',
            deleteBtn: 'delete-contact',
            saveBtn: 'save-contact',
            formId: 'add-contact-form',
            loading: 'loading',
            searchForm: 'search-contact',
            deleteUrl: '/delete-contact',
            redirectUrl: '/contacts',
            searchUrl: '/contacts/search/'
        };
        var mainCtrl = new MainCtrl(config);
        mainCtrl.initialize();
    });
</script>