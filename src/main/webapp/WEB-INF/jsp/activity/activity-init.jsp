<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<script type="text/javascript" src="${pageContext.servletContext.contextPath}/html/js/main.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        var config = {
            containerId: 'activity-container',
            deleteDialog: 'deleteDialog',
            deleteBtn: 'delete-activity',
            saveBtn: 'save-activity',
            formId: 'add-activity-form',
            loading: 'loading',
            searchForm: 'search-activity',
            deleteUrl: '/delete-activity',
            redirectUrl: '/activities',
            searchUrl: '/activities/search/'
        };

        var mainCtrl = new MainCtrl(config);
        mainCtrl.initialize();

        $("#datepicker").datepicker();
    });
</script>