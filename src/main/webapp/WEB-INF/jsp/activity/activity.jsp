<%@include file="activity-init.jsp" %>

<div id="activity-container" class="container">

    <div class="row">
        <div class="col-md-4">
            <div class="row form-group">
                <input type="text" value="" class="form-control" placeholder="<spring:message code="search"/>"/>
            </div>
            <div class="row form-group">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th><spring:message code="activity.title"/></th>
                        <th><spring:message code="activity.contact"/></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${activities}" var="activity">
                        <tr>
                            <td><a href="${pageContext.servletContext.contextPath}/activities/${activity.id}">${activity.title}</a></td>
                            <td><a href="${pageContext.servletContext.contextPath}/activities/${activity.id}">${activity.contact.firstName} ${activity.contact.firstName}</a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <a href="${pageContext.servletContext.contextPath}/activities/add-activity" id="add-contact" class="btn btn-info"><spring:message code="add.activity"/></a>
            </div>
        </div>
        <div class="col-md-8">
            <c:if test="${showAddActivityView}">
                <%@include file="add-activity.jsp" %>
            </c:if>
        </div>

    </div>
</div>
