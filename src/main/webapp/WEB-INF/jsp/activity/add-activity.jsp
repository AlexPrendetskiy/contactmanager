<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="container">
    <div class="row">
        <c:if test="${activityId ne null}">
            <a href="javascript:;" id="deleteDialog" data-id="${activityId}" data-toggle="modal"
               class="btn btn-danger"><spring:message code="delete.activity"/></a>
        </c:if>
        <a href="javascript:;" id="save-activity" class="btn btn-info"><spring:message code="save.activity"/></a>
    </div>
    <br/>
    <div class="row">
        <form:form id="add-activity-form" modelAttribute="activity" action="/save-or-update-activity" method="post">
            <form:hidden path="id"/>

            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="activity.contact"/></label>
                <div class="col-sm-4">
                    <form:select path="contact.id" id="contact" cssClass="form-control">
                        <option value=""><spring:message code="empty" /></option>
                        <c:forEach items="${contacts}" var="ct">
                            <option value="${ct.id}"
                                    <c:if test="${ct.id == activity.contact.id or ct.id == contactId}">
                                        selected
                                    </c:if>
                            >
                                    ${ct.firstName} ${ct.lastName}
                            </option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="activity.type"/></label>
                <div class="col-sm-4">
                    <form:select path="activityType" id="contact" cssClass="form-control">
                        <option value="${null}"><spring:message code="any" /></option>
                        <c:forEach items="${types}" var="type">
                            <option value="${type}"
                                    <c:if test="${type eq activity.activityType}">
                                        selected
                                    </c:if>
                            >
                                    ${type}
                            </option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="activity.subject"/></label>
                <div class="col-sm-4">
                    <form:input path="title" cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="activity.due.date"/></label>
                <div class="col-sm-4">
                    <form:input path="dueDate" id="datepicker" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 form-control-label"><spring:message code="activity.notes"/></label>
                <div class="col-sm-4">
                    <form:textarea path="notes" cssClass="form-control"/>
                </div>
            </div>

        </form:form>
    </div>

    <div id="confirmationDialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><spring:message code="delete.activity"/></h4>
                </div>
                <div class="modal-body">
                    <p><spring:message code="delete.activity.confirmation.message"/></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message
                            code="no"/></button>
                    <button type="button" id="delete-activity" data-dismiss="modal" class="btn btn-default">
                        <spring:message code="yes"/></button>
                </div>
            </div>

        </div>
    </div>

</div>
