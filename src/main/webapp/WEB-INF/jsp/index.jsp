<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<!DOCTYPE html>
<html>
<head>
    <title><spring:message code="title"/></title>

    <script type="text/javascript"
            src="${pageContext.servletContext.contextPath}/html/js/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.servletContext.contextPath}/html/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="${pageContext.servletContext.contextPath}/html/js/bootstrap.min.js"></script>

    <link type=text/css rel="stylesheet" href="${pageContext.servletContext.contextPath}/html/css/jquery-ui.css">
    <link type=text/css rel="stylesheet" href="${pageContext.servletContext.contextPath}/html/css/bootstrap.min.css">
    <link type=text/css rel="stylesheet" href="${pageContext.servletContext.contextPath}/html/css/main.css"/>

</head>
<body>

<div class="container">
    <div class="row">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/"><spring:message code="technical.test.crm.label"/></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="${pageContext.servletContext.contextPath}/contacts"><spring:message code="contacts"/></a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/activities"><spring:message code="activity"/></a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    <div id="loading" style="display: none">
        <img src="${pageContext.servletContext.contextPath}/html/images/loading.gif" alt="Loading.."/>
        <h2 style="color:gray;font-weight:normal;">Please wait....</h2>
    </div>

</div>

<tiles:insertAttribute name="content"/>
</body>
</html>

