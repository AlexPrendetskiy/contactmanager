package com.aimprosoft.contactmanager.common.util;

import com.aimprosoft.contactmanager.common.model.Activity;
import com.aimprosoft.contactmanager.common.model.ui.ActivityUI;
import org.springframework.beans.BeanUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EntityModelConverter {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

    public static ActivityUI convertToActivityUI(Activity activity) {
        ActivityUI activityUI = new ActivityUI();
        Date date = activity.getDueDate();

        BeanUtils.copyProperties(activity, activityUI, new String[]{});
        activityUI.setDueDate(dateFormat.format(date));

        return activityUI;
    }

    public static Activity convertToActivity(ActivityUI activityUI) {
        Activity activity = new Activity();

        BeanUtils.copyProperties(activityUI, activity, new String[]{});
        try {
            activity.setDueDate(dateFormat.parse(activityUI.getDueDate()));
        } catch (ParseException e) {
            activity.setDueDate(new Date());
        }

        return activity;
    }

}
