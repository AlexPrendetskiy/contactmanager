package com.aimprosoft.contactmanager.common.persistence;

import com.aimprosoft.contactmanager.common.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(propagation = Propagation.REQUIRED)
public interface ContactPersistence extends JpaRepository<Contact, Long> {

    @Query(value = "FROM Contact c WHERE c.firstName like CONCAT('%', :query, '%') or c.lastName like CONCAT('%', :query, '%') order by c.firstName  ")
    List<Contact> searchByQuery(@Param("query") String query);

}
