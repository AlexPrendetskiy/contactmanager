package com.aimprosoft.contactmanager.common.persistence;

import com.aimprosoft.contactmanager.common.model.Activity;
import com.aimprosoft.contactmanager.common.model.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Transactional(propagation = Propagation.REQUIRED)
public interface ActivityPersistence extends JpaRepository<Activity, Long> {

    @Query(value = "FROM Activity a WHERE a.contact.id=:id")
    List<Activity> findByContactId(@Param("id") Long id);

    @Query(value = "FROM Activity a WHERE a.title like CONCAT('%', :query, '%') order by a.title")
    List<Activity> searchByQuery(@Param("query") String query);

}
