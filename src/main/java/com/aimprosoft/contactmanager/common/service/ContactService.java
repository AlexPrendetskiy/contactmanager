package com.aimprosoft.contactmanager.common.service;

import com.aimprosoft.contactmanager.common.model.Contact;

import java.util.List;

public interface ContactService {

    List<Contact> getContacts();

    List<Contact> search(String query);

    Contact getContactById(Long contactId);

    void saveOrUpdateContact(Contact contact);

    void deleteContact(Long contactId);
}
