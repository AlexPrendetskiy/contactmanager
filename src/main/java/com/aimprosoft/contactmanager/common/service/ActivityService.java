package com.aimprosoft.contactmanager.common.service;

import com.aimprosoft.contactmanager.common.model.Activity;

import java.util.List;

public interface ActivityService {

    List<Activity> getActivities();

    List<Activity> getActivityByContact(Long contactId);

    List<Activity> search(String query);

    Activity getActivityById(Long activityId);

    void saveOrUpdateActivity(Activity activity);

    void deleteActivity(Long activityId);
}
