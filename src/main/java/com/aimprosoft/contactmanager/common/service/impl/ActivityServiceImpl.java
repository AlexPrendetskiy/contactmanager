package com.aimprosoft.contactmanager.common.service.impl;

import com.aimprosoft.contactmanager.common.model.Activity;
import com.aimprosoft.contactmanager.common.persistence.ActivityPersistence;
import com.aimprosoft.contactmanager.common.service.ActivityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService {

    private Logger _logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ActivityPersistence activityPersistence;

    @Override
    public List<Activity> getActivities() {
        return activityPersistence.findAll();
    }

    @Override
    public List<Activity> getActivityByContact(Long contactId) {
        return activityPersistence.findByContactId(contactId);
    }

    @Override
    public List<Activity> search(String query) {
        return activityPersistence.searchByQuery(query);
    }

    @Override
    public Activity getActivityById(Long activityId) {
        return activityPersistence.findOne(activityId);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveOrUpdateActivity(Activity activity) {
        activityPersistence.save(activity);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteActivity(Long activityId) {
        activityPersistence.delete(activityId);
    }
}
