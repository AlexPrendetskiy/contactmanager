package com.aimprosoft.contactmanager.common.service.impl;

import com.aimprosoft.contactmanager.common.model.Activity;
import com.aimprosoft.contactmanager.common.model.Contact;
import com.aimprosoft.contactmanager.common.persistence.ActivityPersistence;
import com.aimprosoft.contactmanager.common.persistence.ContactPersistence;
import com.aimprosoft.contactmanager.common.service.ContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {

    private Logger _logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ContactPersistence contactPersistence;

    @Autowired
    private ActivityPersistence activityPersistence;

    @Override
    public List<Contact> getContacts() {
        return contactPersistence.findAll();
    }

    @Override
    public List<Contact> search(String query) {
        return contactPersistence.searchByQuery(query);
    }

    @Override
    public Contact getContactById(Long contactId) {
        return contactPersistence.findOne(contactId);
    }

    @Override
    @Transactional(readOnly = false)
    public void saveOrUpdateContact(Contact contact) {
        List<Activity> activities = activityPersistence.findByContactId(contact.getId());
        contact.setActivities(activities);

        contactPersistence.save(contact);
    }

    @Override
    @Transactional(readOnly = false)
    public void deleteContact(Long contactId) {
        contactPersistence.delete(contactId);
    }
}
