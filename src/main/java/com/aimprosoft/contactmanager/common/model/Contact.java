package com.aimprosoft.contactmanager.common.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "contact")
public class Contact extends BusinessModel {

    @Column(name = "firstName", nullable = false ,unique = false)
    private String firstName;

    @Column(name = "lastName", nullable = false ,unique = false)
    private String lastName;

    @Column(name = "email", nullable = false, unique = true)
    private String emailAddress;

    @Column(name = "phoneNumber", nullable = true, unique = true)
    private String phoneNumber;

    @Column(name = "address1", nullable = true, unique = false)
    private String address1;

    @Column(name = "address2", nullable = true, unique = false)
    private String address2;

    @Column(name = "city", nullable = true, unique = false)
    private String city;

    @Column(name = "postCode", nullable = true, unique = false)
    private String postCode;

    @OneToMany(mappedBy = "contact", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Activity> activities;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }
}
