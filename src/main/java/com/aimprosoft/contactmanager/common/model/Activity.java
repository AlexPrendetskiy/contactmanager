package com.aimprosoft.contactmanager.common.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "activity")
public class Activity extends BusinessModel {

    @Column(name = "title", nullable = true ,unique = false)
    private String title;

    @Column(name = "notes", nullable = true ,unique = false)
    private String notes;

    @Column(name = "dueDate", nullable = false ,unique = false)
    @Temporal(value = TemporalType.DATE)
    private Date dueDate;

    @Column(name = "activityType", nullable = true)
    @Enumerated(EnumType.STRING)
    private ActivityType activityType;

    @ManyToOne
    @JoinColumn(name = "contactId", nullable = true)
    private Contact contact;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }
}
