package com.aimprosoft.contactmanager.common.model;

public enum ActivityType {
    Call, Email, Meeting
}
