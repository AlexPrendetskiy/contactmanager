package com.aimprosoft.contactmanager.servlet.controller;

import com.aimprosoft.contactmanager.common.service.ActivityService;
import com.aimprosoft.contactmanager.common.service.ContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

public abstract class BaseController {

    protected Logger _logger = LoggerFactory.getLogger(getClass());

    protected static final String DEFAULT_SUCCESS_RESULT = "{\"success\":true}";

    protected static final String DEFAULT_ERROR_RESULT = "{\"success\":false}";

    @Autowired
    protected ContactService contactService;

    @Autowired
    protected ActivityService activityService;

    protected Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }


}
