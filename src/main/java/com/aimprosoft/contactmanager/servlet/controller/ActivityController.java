package com.aimprosoft.contactmanager.servlet.controller;

import com.aimprosoft.contactmanager.common.model.Activity;
import com.aimprosoft.contactmanager.common.model.ActivityType;
import com.aimprosoft.contactmanager.common.model.Contact;
import com.aimprosoft.contactmanager.common.model.ui.ActivityUI;
import com.aimprosoft.contactmanager.common.util.EntityModelConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ActivityController extends BaseController {

    @RequestMapping(value = "activities")
    public String index(Model model, HttpServletRequest request) {
        List<Activity> activities = activityService.getActivities();
        model.addAttribute("activities", activities);
        model.addAttribute("showAddActivityView", false);

        return "activity";
    }

    @RequestMapping(value = "/activities/search/{query}", method = RequestMethod.GET)
    public String searchActivity(@PathVariable("query") String query, Model model) {
        List<Activity> activities = activityService.search(query);

        model.addAttribute("activities", activities);
        model.addAttribute("showAddActivityView", false);

        return "activity";
    }

    @RequestMapping(value = "/activities/{activityId}", method = RequestMethod.GET)
    public String getActivity(@PathVariable("activityId") Long activityId,
                             Model model, HttpServletRequest request) {

        List<Activity> activities = activityService.getActivities();
        List<Contact> contacts = contactService.getContacts();

        Activity activity = activityService.getActivityById(activityId);

        model.addAttribute("activities", activities);
        model.addAttribute("activityId", activityId);
        model.addAttribute("showAddActivityView", true);
        model.addAttribute("activity", EntityModelConverter.convertToActivityUI(activity));
        model.addAttribute("contacts", contacts);
        model.addAttribute("types", ActivityType.values());

        return "activity";
    }

    @RequestMapping(value = "/activities/add-activity-to-contact/{contactId}", method = RequestMethod.GET)
    public String addActivityToContact(@PathVariable("contactId") Long contactId,
                              Model model, HttpServletRequest request) {

        List<Activity> activities = activityService.getActivities();
        List<Contact> contacts = contactService.getContacts();

        model.addAttribute("activities", activities);
        model.addAttribute("showAddActivityView", true);
        model.addAttribute("activity", new Activity());
        model.addAttribute("contacts", contacts);
        model.addAttribute("contactId", contactId);
        model.addAttribute("types", ActivityType.values());

        return "activity";
    }

    @RequestMapping(value = "/activities/add-activity", method = RequestMethod.GET)
    public String addActivity(Model model, HttpServletRequest request) {
        List<Activity> activities = activityService.getActivities();
        List<Contact> contacts = contactService.getContacts();

        model.addAttribute("activities", activities);
        model.addAttribute("showAddActivityView", true);
        model.addAttribute("activity", new Activity());
        model.addAttribute("activityId", null);
        model.addAttribute("contacts", contacts);
        model.addAttribute("types", ActivityType.values());

        return "activity";
    }

    @RequestMapping(value = "save-or-update-activity", method = RequestMethod.POST)
    public String saveContact(@ModelAttribute("activity") ActivityUI activity) {

        // TODO add validation
        activityService.saveOrUpdateActivity(EntityModelConverter.convertToActivity(activity));

        return "redirect:/activities";
    }

    @RequestMapping(value = "delete-activity", method = RequestMethod.POST)
    public String deleteContact(@RequestParam(value = "id", required = true) Long activityId) {

        activityService.deleteActivity(activityId);

        return "redirect:/activities";
    }

}
