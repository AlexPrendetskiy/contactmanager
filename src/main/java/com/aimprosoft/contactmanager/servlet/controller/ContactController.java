package com.aimprosoft.contactmanager.servlet.controller;

import com.aimprosoft.contactmanager.common.model.Activity;
import com.aimprosoft.contactmanager.common.model.Contact;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ContactController extends BaseController {

    @RequestMapping(value = "contacts")
    public String index(Model model, HttpServletRequest request) {
        List<Contact> contacts = contactService.getContacts();
        model.addAttribute("contacts", contacts);
        model.addAttribute("showAddContactView", false);

        return "contacts";
    }

    @RequestMapping(value = "/contacts/{contactId}", method = RequestMethod.GET)
    public String getContact(@PathVariable("contactId") Long contactId,
                             Model model, HttpServletRequest request) {
        List<Contact> contacts = contactService.getContacts();
        List<Activity> activities = activityService.getActivities();
        Contact contact = contactService.getContactById(contactId);

        model.addAttribute("activities", activities);
        model.addAttribute("contacts", contacts);
        model.addAttribute("contactId", contactId);
        model.addAttribute("showAddContactView", true);
        model.addAttribute("contact", contact);

        return "contacts";
    }

    @RequestMapping(value = "/contacts/search/{query}", method = RequestMethod.GET)
    public String searchContact(@PathVariable("query") String query, Model model) {
        List<Contact> contacts = contactService.search(query);

        model.addAttribute("contacts", contacts);
        model.addAttribute("showAddContactView", false);

        return "contacts";
    }

    @RequestMapping(value = "/contacts/add-contact", method = RequestMethod.GET)
    public String addContact(Model model) {
        List<Contact> contacts = contactService.getContacts();
        List<Activity> activities = activityService.getActivities();

        model.addAttribute("activities", activities);
        model.addAttribute("contacts", contacts);
        model.addAttribute("showAddContactView", true);
        model.addAttribute("contact", new Contact());
        model.addAttribute("contactId", null);

        return "contacts";
    }

    @RequestMapping(value = "save-or-update-contact", method = RequestMethod.POST)
    public String saveContact(@ModelAttribute("contact") Contact contact) {

        // TODO add validation
        contactService.saveOrUpdateContact(contact);

        return "redirect:/contacts";
    }

    @RequestMapping(value = "delete-contact", method = RequestMethod.POST)
    public String deleteContact(@RequestParam(value = "id", required = true) Long contactId) {

        contactService.deleteContact(contactId);

        return "redirect:/contacts";
    }
}
