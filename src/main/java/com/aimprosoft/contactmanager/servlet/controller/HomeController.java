package com.aimprosoft.contactmanager.servlet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController extends BaseController {

    @RequestMapping({"/", "index.html"})
    public String index() {
        return "main";
    }

}
