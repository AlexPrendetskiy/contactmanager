## Contact Manager (Technical Test Project)

**Installation Instructions**
- run /src/main/java/com/aimprosoft/JettyMain.java (or run **start.bat** or **start.sh**)  


Here is stack of Java-based technologies:

- [Jetty](http://www.eclipse.org/jetty/) &mdash; embedded web-container;

- [Spring](http://www.springsource.org/) &mdash; IoC/DI/AOP container, as well as MVC-framework;

- [Spring Data JPA](http://www.springsource.org/spring-data/jpa) &mdash; JPA persistence;

- [Hibernate](http://www.hibernate.org/) &mdash; JPA-vendor;

- [jQuery](http://jquery.com/) &mdash; main cross-browser framework;

- [Bootstrap](http://getbootstrap.com/) &mdash; popular HTML, CSS, and JS framework

### Features List ###

1 Add / Edit / Remove Contacts (People) As a user I should be able add, edit and remove Contacts - **Done**
2 View Contacts in a Listing As a user I should be able to view all Contacts in the system - **Done** 
3 Add / Edit / Remove Activities As a user I should be able to add, edit and remove - **Done** 
4 View Contacts in a Listing As a user I should be able to view all Activities - **Done**
5 View Activities Listing on a Contact page As a user I should be able to view all Activities related to a Contact on the Contact Page - **Done**
6 Add Activities from a Contact page As a user I should be able to add an Activity from a Contact page and the Contact details be automatically populated - **Done** 
7 Validation of Contact data As a user I should not be able to add a Contact without first name, last name and email address - **Not implemented**  
8 Validation of Activity data As a user I should be able to add an Activity without subject, contact and type - **Not implemented** 
9 Search Contacts As a user I should be able to text search Contacts by first name / last name - **Implemented partially (http://localhost:8080/contacts/search/{query})**
10 Search Activities As a user I should be able to text search Activities by subject - **Implemented partially (http://localhost:8080/actinities/search/{query})** 


